import unittest
from optimizer import Optimizer

class TestHStackFunctions(unittest.TestCase):
    def setUp(self):
        f = open('input/0', 'r')
        f1 = open('input/1', 'r')
        f2 = open('input/2', 'r')
        f3 = open('input/3', 'r')
        self.graph = f.readlines() 
        self.graph1 = f1.readlines() 
        self.graph2 = f2.readlines() 
        self.graph3 = f3.readlines() 
        self.optimizer = Optimizer(self.graph)
        self.o1 = Optimizer(self.graph1)
        self.o2 = Optimizer(self.graph2)
        self.o3 = Optimizer(self.graph3)

    def test_initialize(self):
        graph = self.optimizer._graph
        graph1 = self.o1._graph
        self.assertEqual(graph.keys(), ["A", "B"])
        self.assertEqual(graph.values(), [["B"], ["C"]])
        self.assertEqual(graph1.keys(), ["A", "B", "D"])

    def test_one_child(self):
        actual = self.optimizer.one_child()
        expected = ["A", "B"]
        self.assertEqual(actual, expected)
        actual = self.o3.one_child()
        expected = ["A"]
        self.assertEqual(actual, expected)

    def test_one_parent(self):
        actual = self.optimizer.one_parent()
        expected = ["B", "C"]
        self.assertEqual(actual, expected)
        actual = self.o3.one_parent()
        expected = ["B", "C", "D"]
        self.assertEqual(actual, expected)

    def test_redundants(self):
        actual = self.optimizer.redundants()
        expected = ["B"]
        self.assertEqual(actual, expected)

    def test_find_parent(self):
        actual = self.optimizer.find_parent("B")
        expected = "A"
        self.assertEqual(actual, expected)

    def test_optimize(self):
        self.optimizer.optimize()
        self.o1.optimize()
        self.o2.optimize()
        self.o3.optimize()

        actual_keys = self.optimizer._graph.keys()
        expected_keys = ["A"]
        self.assertEqual(actual_keys, expected_keys)

        actual_keys = self.o1._graph.keys()
        expected_keys = ["A"]
        self.assertEqual(actual_keys, expected_keys)

        actual_keys = self.o2._graph.keys()
        expected_keys = []
        self.assertEqual(actual_keys, expected_keys)

        actual_keys = self.o3._graph.keys()
        expected_keys = ["A", "B"]
        self.assertEqual(actual_keys, expected_keys)

    def test_output(self):
        output = open('output/0', 'r')
        expected = output.readlines()
        actual = self.optimizer.output()
        self.assertEqual(actual, expected)

        output = open('output/1', 'r')
        expected = output.readlines()
        actual = self.o1.output()
        self.assertEqual(actual, expected)

        output = open('output/2', 'r')
        expected = output.readlines()
        actual = self.o2.output()
        self.assertEqual(actual, expected)
