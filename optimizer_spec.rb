require_relative 'optimizer'

describe Optimizer do
  let(:input){ File.readlines('./input/0') }
  let(:optimizer){ Optimizer.new input }
  let(:output){ File.readlines('./output/0') }

  let(:input1) { File.readlines('./input/1') }
  let(:o1){ Optimizer.new input1 }
  let(:output1){ File.readlines('./output/1') }

  let(:input2) { File.readlines('./input/2') }
  let(:o2){ Optimizer.new input2 }
  let(:output2){ File.readlines('./output/2') }

  let(:input3) { File.readlines('./input/3') }
  let(:o3){ Optimizer.new input3 }

  context 'initialize' do
    it 'sets graph' do
      expect(optimizer.graph.keys).to eq %w[A B]
      expect(optimizer.graph.values.flatten).to eq %w[B C]
      expect(o3.graph.values.flatten).to eq %w[B C D]
    end
  end

  context 'one_child' do
    it 'returns all nodes with one child' do
      expect(optimizer.one_child).to eq %w[A B]
      expect(o3.one_child).to eq %w[A]
    end
  end

  context 'one_parent' do
    it 'returns all nodes with one parent' do
      expect(optimizer.one_parent).to eq %w[B C]
      expect(o3.one_parent).to eq %w[B C D]
    end
  end

  context 'redundants' do
    it 'returns all nodes with one parent and one child' do
      expect(optimizer.redundants).to eq %w[B]
      expect(o1.redundants).to eq %w[B D]
      expect(o2.redundants).to eq %w[A B C D]
      expect(o3.redundants).to eq []
    end
  end

  context 'find parent' do
    it 'returns parent node of a given value' do
      expect(optimizer.find_parent "B").to eq "A"
    end
  end

  context 'optimize' do
    it 'eliminates redundant node and creates new edge' do
      optimizer.optimize
      o1.optimize
      o2.optimize
      o3.optimize

      expect(optimizer.graph.keys).to eq %w[A]
      expect(optimizer.graph.keys.count).to eq 1
      
      expect(o1.graph.keys).to eq %w[A]
      expect(o1.graph.values.flatten).to eq %w[C]
      expect(o1.graph.keys.count).to eq 1

      expect(o2.graph.keys.count).to eq 0
      expect(o2.graph).to be_empty

      expect(o3.graph.keys.count).to eq 2
      expect(o3.graph.keys).to eq %w[A B]
    end
  end

  context 'output' do
    it 'formats graph in output file' do
      expect(optimizer.output).to eq output
      expect(o1.output).to eq output1
      expect(o2.output).to eq output2
    end
  end
end
