#! /usr/bin/python

import sys
import re

class Optimizer():
    def __init__(self, graph):
        parser = re.compile('(?P<key>[A-Z])(.*)(?P<value>[A-Z])')
        self._graph = {}
        for line in graph:
            m = parser.match(line)
            key = m.group('key')
            value = m.group('value')
            if key in self._graph:
                self._graph[key].append(value)
            else:
                self._graph[key] = [value]

    def one_child(self):
        '''returns nodes with only one child'''
        output = []
        keys = self._graph.keys()
        for key in keys:
            value = self._graph[key]
            if len(value) == 1:
                output.append(key)
        return output

    def one_parent(self):
        '''returns nodes with only one parent'''
        values = self._graph.values()
        children = [child for value in values for child in value]
        output = []
        for child in children:
            if children.count(child) == 1:
                output.append(child)
        return output

    def redundants(self):
        ''' returns all potentially redundant nodes '''
        children = set(self.one_child())
        parents = set(self.one_parent())
        return list(children & parents)

    def find_parent(self, node):
        ''' finds parent node for given child node'''
        for key, value in self._graph.items():
            if node in value:
                return key

    def optimize(self):
        ''' removes redundant nodes from graph '''
        reds = self.redundants()
        for node in reds:
            child = self._graph[node][0]
            new_parent = self.find_parent(node)
            self._graph[new_parent].remove(node)
            if not child in self._graph[new_parent]:
                self._graph[new_parent].append(child)
            del self._graph[node]

    def output(self):
        ''' returns list of formatted strings '''
        self.optimize()
        lines = []
        for k, v in self._graph.items():
            for child in v:
                line = "%s\t%s\n" %(k, child)
                lines.append(line)
        return lines

if __name__ == "__main__":
    f = open(sys.argv[1])
    graph = f.readlines()
    optimizer = Optimizer(graph)
    for line in  optimizer.output():
        print line
