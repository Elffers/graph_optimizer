#!/usr/bin/env ruby

class Optimizer
  attr_accessor :graph

  def initialize input
    @graph = Hash.new { |hash, key| hash[key] = [] }
    input.each do |line|
      /(?<key>[A-Z]).*(?<value>[A-Z])/ =~ line
      @graph[key].push value
    end
  end

  # returns all nodes (keys in @graph) with only one child
  def one_child
    @graph.keys.select do |key|
      graph[key].count == 1
    end
  end

  # returns all nodes (keys in @graph) with only one parent
  def one_parent
    values = @graph.values.flatten
    values.select {|value| values.count(value) == 1 }
  end

  # returns all nodes that can potentially be deleted from the graph
  def redundants
    one_child & one_parent
  end

  # This would only be called on node that is known to have a single parent
  # TODO: improve current efficiency (quadratic)
  def find_parent node
    @graph.each do |parent,children|
      return parent if children.include? node
    end
  end

  # removes redundant nodes from graph
  def optimize
    redundants.each do |node|
      child = @graph[node].first # grabs the unique child node
      new_parent = find_parent node
      @graph[new_parent].delete node
      @graph[new_parent].push child unless @graph[new_parent].include? child
      @graph.delete node
    end
  end

  def output
    optimize
    lines = []
    @graph.each do |k, v|
      v.each do |child|
        line = "#{k}\t#{child}\n"
        lines.push line
      end
    end
    lines
  end
end

if $0 == __FILE__
  input = ARGF.each_line
  optimizer = Optimizer.new input
  puts optimizer.output
end
